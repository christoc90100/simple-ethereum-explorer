import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import {timeout} from 'rxjs/operators';
import {Observable} from 'rxjs';

export class TimeoutInterceptor implements HttpInterceptor {

  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const API_TIMEOUT = 3000; // 3 seconds
    return next.handle(req).pipe(timeout(API_TIMEOUT));
  }
}
