import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {EthereumService} from './service/ethereum/ethereum.service';
import {TimeoutInterceptor} from './interceptor/timeout.interceptor';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    CommonModule
  ],
  providers: [
    {
      provide: EthereumService,
      useClass: EthereumService,
      deps: [HttpClient]
    },
    { provide: HTTP_INTERCEPTORS,
      useClass: TimeoutInterceptor,
      multi: true
    }
  ]
})
export class CoreModule { }
