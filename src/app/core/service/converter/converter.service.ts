import { Injectable } from '@angular/core';
import {Transaction} from '../../../shared/dto/Transaction';
import {TransactionRow} from '../../../shared/model/TransactionRow';

@Injectable({
  providedIn: 'root'
})
export class ConverterService {

  constructor() { }

  /* Converts Transaction DTO into the Rows which are displayed in the table. */
  convertToTableRows(transactions: Transaction[], searchAddress: string): TransactionRow[] {
    if (!transactions) {
      return undefined;
    }
    const rows: TransactionRow[] = [];
    for (const transaction of transactions) {
      const isDebit = this.isDebit(transaction.to, searchAddress);
      const rowAddress = this.getDisplayAddress(transaction.to, transaction.from, isDebit);
      const convertedValue = this.fromWeiToNumber(transaction.value);
      rows.push(new TransactionRow(
        this.fromTimestampToDate(transaction.timeStamp),
        this.fromTimestampToTime(transaction.timeStamp),
        rowAddress,
        this.getShortAddress(rowAddress),
        this.getMidAddress(rowAddress),
        transaction.tokenName,
        transaction.tokenSymbol,
        transaction.value,
        this.formatValueTransfered(isDebit, convertedValue),
        isDebit
      ));
    }
    return rows;
  }

  private isDebit(toAddress: string, searchedAddress: string): boolean {
    return toAddress.toLowerCase() === searchedAddress;
  }

  private getDisplayAddress(toAddress: string, fromAddress: string, isDebit: boolean): string {
    return isDebit ? fromAddress : toAddress;
  }

  /* API does not distinguish credits from debits.  We have to manually check.
  *  Also formats number with commas.*/
  formatValueTransfered(isDebit: boolean, value: number|string): string {
    value = String(value);
    const commaRegex = /\B(?=(\d{3})+(?!\d))/g;
    const split = value.split('.');
    const sigDigits = split[1];
    if (sigDigits && sigDigits.length > 4) {
      value = value.split('.')[0].replace(commaRegex, ',')
        + '.' + value.split('.')[1].substr(0, 4) + '...';
    } else if (sigDigits) {
      value = value.split('.')[0].replace(commaRegex, ',')
        + '.' + value.split('.')[1];
    }
    return isDebit ? '+' + value : '-' + value;
  }

  fromTimestampToTime(value: string): string {
    if (!value) {
      return undefined;
    }
    const date = new Date(Number(value) * 1000);
    return date.toLocaleTimeString();

  }

  private fromTimestampToDate(value: string): string {
    if (!value) {
      return undefined;
    }
    const date = new Date(Number(value) * 1000);
    return date.toLocaleDateString();
  }

  /* Ethereum API we're using returns all values in wei, so this converts it to user-friendly numbers. */
  fromWeiToNumber(value: number|string): number {
    if (!value || isNaN(Number(value))) {
      return undefined;
    }
    return Number(value) / (Math.pow(10, 18));
  }

  private getMidAddress(address: string): string {
    if (!address) {
      return undefined;
    }
    return address.substr(0, 18) + '...';
  }

  private getShortAddress(address: string): string {
    if (!address) {
      return undefined;
    }
    return address.substr(0, 9) + '...';
  }
}
