import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ApiResponse} from '../../../shared/dto/ApiResponse';
import {Transaction} from '../../../shared/dto/Transaction';
import {TimeoutError} from 'rxjs/internal/util/TimeoutError';

@Injectable({
  providedIn: 'root'
})
export class EthereumService {

  constructor(private http: HttpClient) { }

  lookupAddress(address: string): Promise<Transaction[]> {
    let params = new HttpParams();
    params = params.append('module', 'account');
    params = params.append('action', 'tokentx');
    params = params.append('address', address);
    params = params.append('startblock', '0');
    params = params.append('endblock', '999999999');
    params = params.append('sort', 'asc');
    params = params.append('apikey', '');
    return new Promise((resolve, reject) => {
      this.http.get('https://api.etherscan.io/api', { params }).toPromise()
        .then((response: ApiResponse) => {
          if (response && response.result && response.status !== '0') {
            resolve(response.result);
          } else {
            reject(new EthereumServiceError('Cannot parse response data.', false));
          }
        })
        .catch((err) => {
          if (this.isTimeoutError(err)) {
            reject(new EthereumServiceError(
              'Please search an address with less transfer activity, or try again.', true));
          }
          reject(new EthereumServiceError('Could not fetch transactions.', false));
        });
    });
  }

  private isTimeoutError(err: any): boolean {
    return err && err.name && err.name === 'TimeoutError';
  }
}

export class EthereumServiceError {
  constructor(public error: string, public timeout: boolean) {}
}
