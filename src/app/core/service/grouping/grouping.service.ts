import {Injectable} from '@angular/core';
import {TransactionRow} from '../../../shared/model/TransactionRow';

@Injectable({
  providedIn: 'root'
})
export class GroupingService {

  groupBySymbol(transactions: TransactionRow[]): Map<string, TransactionRow[]> {
    const map = new Map<string, TransactionRow[]>();
    if (!transactions || transactions.length === 0) {
      return map;
    }
    for (const transaction of transactions) {
      const symbol = transaction.tokenName;
      map.has(symbol) ? map.get(symbol).push(transaction) : map.set(symbol, [transaction]);
    }
    return map;
  }

  calcTokenTotals(transactions: TransactionRow[]): Map<string, number> {
    const map = new Map<string, number>();
    if (!transactions || transactions.length === 0) {
      return map;
    }
    for (const transaction of transactions) {
      const symbol = transaction.tokenName;
      const val = Number(transaction.value);
      if (!isNaN(val)) {
        const entryValue = transaction.isDebit ? val : -val;
        map.has(symbol) ?
          map.set(symbol, entryValue+map.get(symbol)) :
          map.set(symbol, entryValue);
      }
    }
    return map;
  }
}
