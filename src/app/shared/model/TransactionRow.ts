export class TransactionRow {
  constructor(
    public date: string,
    public time: string,
    public address: string,
    public shortAddress: string,
    public midAddress: string,
    public tokenName: string,
    public tokenSymbol: string,
    public value: string,
    public formattedValue: string,
    public isDebit: boolean) {}
}
