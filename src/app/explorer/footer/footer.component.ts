import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  @Input() isShowingHelp: boolean;
  @Input() isMobile: boolean;
  @Output() showHelp: EventEmitter<void> = new EventEmitter();
}
