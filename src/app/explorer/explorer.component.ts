import {Component, OnInit, ViewChild} from '@angular/core';
import {EthereumService, EthereumServiceError} from '../core/service/ethereum/ethereum.service';
import {Transaction} from '../shared/dto/Transaction';
import {GroupingService} from '../core/service/grouping/grouping.service';
import {ConverterService} from '../core/service/converter/converter.service';
import {MatDialog, MatPaginator, MatSelectChange, MatTableDataSource} from '@angular/material';
import {ExplorerDialogComponent} from './dialog/explorer-dialog.component';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Location} from '@angular/common';
import {routeNames} from '../shared/model/routeNames';
import {TransactionRow} from '../shared/model/TransactionRow';

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.scss']
})
export class ExplorerComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  isLoading: boolean;
  isMobile: boolean;
  showAuditTrail: boolean = false;
  isShowingHelp: boolean = true;
  address: string;
  searchedAddress: string;
  tokenTotal: number;
  tokens: string[] = [];
  displayedColumns: string[] = ['rowNumber', 'symbol', 'timestamp', 'value', 'toFrom'];
  transactions: TransactionRow[] = [];
  dataSource: MatTableDataSource<TransactionRow>;
  selectedToken: string;
  auditTrail: string[] = [];
  private transactionsMap: Map<string, TransactionRow[]>;
  private tokenTotalsMap: Map<string, number>;

  constructor(private ethereumHttpService: EthereumService,
              private groupingService: GroupingService,
              private converterService: ConverterService,
              private dialog: MatDialog,
              private location: Location,
              private router: Router) { }

  ngOnInit(): void {
    this.listenForScreenResize();
    this.searchIfPossible(this.router.url);
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.searchIfPossible(val.url);
      }
    });
  }

  listenForScreenResize(): void {
    const mobileDivider = 600;
    this.isMobile = window.innerWidth < mobileDivider;
    window.addEventListener('resize', () => {
      this.isMobile = window.innerWidth < mobileDivider;
    });
  }

  /* Called on page load, or when the user hits the search button, or user clicks a hyperlink in the table. */
  search(address: string): void {
    if (this.isLoading || !address) {
      return;
    }

    address = address.trim().toLowerCase();
    this.isLoading = true;
    this.address = address;
    this.ethereumHttpService.lookupAddress(address)
      .then((transactions: Transaction[]) => {
        this.resetData();
        this.isShowingHelp = false;
        this.searchedAddress = address;
        this.updateTokensAndTransactions(transactions, address);
        this.auditTrail.push(address);
        this.router.navigate([routeNames.EXPLORER], { queryParams: { address } });
      })
      .catch((err: EthereumServiceError) => {
        console.error(err);
        this.openDialog(err.error, err.timeout);
      }).then(() => {
        this.isLoading = false;
      });
  }

  setTransactions(event: MatSelectChange|string): void {
    if (!this.transactionsMap || !event) {
      return;
    }
    let key: string;
    if (event instanceof String) {
      key = String(event);
    } else if (event instanceof MatSelectChange) {
      key = event.value;
    }
    this.transactions = this.transactionsMap.get(key);
    this.tokenTotal = this.converterService.fromWeiToNumber(this.tokenTotalsMap.get(key));
    this.updateTableDataSource(this.transactions);
  }

  openDialog(error: string, timeout: boolean): void {
    const dialogRef = this.dialog.open(ExplorerDialogComponent, {
      width: '300px',
      data: {error, timeout}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.showHelp) {
        this.showHelp();
      }
    });
  }

  showHelp(): void {
    this.resetData();
    this.router.navigate([routeNames.EXPLORER]);
  }

  private searchIfPossible(url: string): void {
    const address = url.split('address=')[1];
    if (address) {
      this.search(address);
    } else {
      this.resetData();
    }
  }

  private updateTableDataSource(transactions: TransactionRow[]): void {
    this.dataSource = new MatTableDataSource(transactions);
    this.dataSource.paginator = this.paginator;
    this.paginator._changePageSize(10);
  }

  private updateTokensAndTransactions(transactions: Transaction[], address: string): void {
    this.transactions = this.converterService.convertToTableRows(transactions, address);
    this.transactionsMap = this.groupingService.groupBySymbol(this.transactions);
    this.tokenTotalsMap = this.groupingService.calcTokenTotals(this.transactions);
    this.tokens = Array.from(this.transactionsMap.keys()).sort();
    this.updateTableDataSource(this.transactions);
  }

  private resetData(): void {
    this.selectedToken = undefined;
    this.transactions = [];
    this.isShowingHelp = true;
    this.transactionsMap = undefined;
    this.tokenTotalsMap = undefined;
    this.tokenTotal = undefined;
    this.searchedAddress = undefined;
    this.address = undefined;
    this.tokens = [];
    if (this.paginator) {
      this.paginator.firstPage();
    }
  }
}
