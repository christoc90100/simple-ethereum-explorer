import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExplorerComponent } from './explorer.component';
import {FormsModule} from '@angular/forms';
import {CoreModule} from '../core/core.module';
import {SharedModule} from '../shared/shared.module';
import {ExplorerDialogComponent} from './dialog/explorer-dialog.component';
import { InfoComponent } from './info/info.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    ExplorerComponent,
    ExplorerDialogComponent,
    InfoComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    ExplorerComponent
  ],
  entryComponents: [
    ExplorerDialogComponent
  ],
})
export class ExplorerModule { }
