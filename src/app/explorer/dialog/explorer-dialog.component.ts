import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'explorer-dialog',
  templateUrl: 'explorer-dialog.component.html',
  styleUrls: ['./explorer-dialog.component.scss']
})
export class ExplorerDialogComponent {

  title: string;

  constructor(
    public dialogRef: MatDialogRef<ExplorerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.title = data.timeout ? 'Is this an Exchange?' : 'Unknown Error';
  }

  onDismiss(): void {
    this.dialogRef.close();
  }
}

export interface DialogData {
  error: string,
  timeout: boolean
}
