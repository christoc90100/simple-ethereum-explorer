import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ExplorerComponent} from './explorer/explorer.component';
import {routeNames} from './shared/model/routeNames';

const routes: Routes = [
  { path: '**', redirectTo: 'explorer', pathMatch: 'full' },
  { path: routeNames.EXPLORER, component: ExplorerComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
