import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ExplorerModule} from './explorer/explorer.module';
import {AppRoutingModule} from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ExplorerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
